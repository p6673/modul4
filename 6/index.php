<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Praktkum 6</title>
</head>

<body>
    <div>
        <?php
        $arr = [[1, "Satu"], [2, "Dua"], [3, "Tiga"], [4, "Empat"], [5, "Lima"], [6, "Enam"], [7, "Tujuh"], [8, "Delapan"], [9, "Sembilan"]];
        for ($a = 0; $a < count($arr); $a++) {
            echo "Index ke-$a-0 adalah " . $arr[$a][0] . "<br>";
            echo "Index ke-$a-1 adalah " . $arr[$a][1] . " <br><br>";
        }
        ?>
    </div>
</body>

</html>