<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Praktkum 3</title>
</head>

<body>
    <form action="index.php" method="POST">
        <input type="number" name="num" value="" min="1" max="1000" style="width: 200px;" placeholder="masukkan banyak bintang">
        <button type="submit">Submit</button>
    </form>
    <br>
    <div>
        <?php
        if (isset($_POST['num'])) {
            for ($a = 0; $a < $_POST['num']; $a++) {
                echo "* ";
            }
        }
        ?>
    </div>
</body>

</html>