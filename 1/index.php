<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Praktkum 1</title>
</head>

<body>
    <form action="index.php" method="POST">
        <input type="text" name="angka" value="" placeholder="Masukkan angka">
        <button type="submit">Submit</button>
    </form>

    <div>
        <?php
        if (isset($_POST['angka'])) {
            if (!is_numeric($_POST['angka'])) echo "ini bukan angka";
            elseif ($_POST['angka'] % 2 == 0) echo "ini adalah angka genap";
            elseif ($_POST['angka'] % 2 != 0) echo "ini adalah angka ganjil";
            else echo "entahlah";
        }
        ?>
    </div>
</body>

</html>