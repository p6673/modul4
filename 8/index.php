<?php
session_start();
if (!isset($_SESSION['username'])) header("Location: login.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Praktkum 8 - Home</title>
</head>

<body>
    <div>
        <h1>Welcome <?= $_SESSION['username'] ?> to Home</h1>
        <button onclick="logout()">logout</button>
    </div>
    <script>
        const logout = () => {
            if (confirm("Apakah anda yakin ?")) window.location.replace("logout.php");
        }
    </script>
</body>

</html>