<?php
session_start();
if (!isset($_SESSION['username'])) {
    if (isset($_POST["username"])) $_SESSION['username'] = $_POST["username"];
    else header("Location: login.php");
}
header("Location: index.php");
