<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Praktkum 4</title>
</head>

<body>
    <form action="index.php" method="POST">
        <input type="number" name="num" value="" min="1" max="20" style="width: 200px;" placeholder="masukkan tinggi segitiga">
        <button type="submit">Submit</button>
    </form>
    <br>
    <div>
        <?php
        if (isset($_POST['num'])) {
            for ($a = 0; $a < $_POST['num']; $a++) {
                for ($b = 0; $b <= $a; $b++) {
                    echo "* ";
                }
                echo "<br>";
            }
        }
        ?>
    </div>
</body>

</html>