<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Praktkum 7</title>
</head>

<body>
    <div>
        <?php
        $a = 1;
        $b = "Satu";
        function ubah(&$aa, $bb)
        {
            $aa = 2;
            $bb = "Dua";
            echo "Variabel a didalam fungsi = $aa<br>";
            echo "Variabel b didalam fungsi = $bb<br><br>";
        };

        echo "Variabel a sebelum diubah = $a <br>";
        echo "Variabel b sebelum diubah = $b <br><br>";

        ubah($a, $b);

        echo "Variabel a setelah diubah = $a<br>";
        echo "Variabel b setelah diubah = $b<br><br>";

        echo "kesimpulannya, variabel fungsi yang menggunakan metode <i>pass by reference</i> akan mempengaruhi variabel aslinya jika ada perubahan didalam fungsi.<br>";
        echo "Sedangkan, pada variabel fungsi yang menggunakan metode <i>pass by value</i> tidak akan mempengaruhi variabel aslinya jika ada perubahan didalam fungsi.";
        ?>
    </div>
</body>

</html>