<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Latihan 3 - Input</title>
</head>

<body>
    <h1>Pilih Band Favorit Anda :</h1>
    <form action="3.proses.php" method="POST">
        <input type="checkbox" name="in1" value="Padi">Padi<br>
        <input type="checkbox" name="in2" value="Sheila on 7">Sheila on 7<br>
        <input type="checkbox" name="in3" value="Dewa 19">Dewa 19<br>
        <input type="checkbox" name="in4" value="Ungu">Ungu<br>
        <button type="submit">Pilih</button>
    </form>
</body>

</html>