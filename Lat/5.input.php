<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Latihan 5 - Input</title>
</head>

<body>
    <h1>Login Here...</h1>
    <form action="5.proses.php" method="POST">
        Username : <input type="text" name="username" required> <br>
        Password : <input type="password" name="password" required> <br>
        <button type="submit">Login</button>
        <button type="reset">Reset</button>
    </form>
</body>

</html>