<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Latihan 4 - Input</title>
</head>

<body>
    <h1>Pilih Film Kartun Favorit Anda :</h1>
    <form action="4.proses.php" method="POST">
        <select name="kartun">
            <option>Sponge Bob</option>
            <option>Sinchan</option>
            <option>Conan</option>
            <option>Doraemon</option>
            <option>Dragon Ball</option>
            <option>Naruto</option>
        </select>
        <button type="submit">Pilih</button>
    </form>
</body>

</html>