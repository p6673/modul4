<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Praktkum 2</title>
</head>

<body>
    <form action="index.php" method="POST">
        <input type="number" name="num" value="" min="1" max="10" placeholder="masukkan angka">
        <button type="submit">Submit</button>
    </form>

    <div>
        <?php
        if (isset($_POST['num'])) {
            switch ($_POST['num']) {
                case 2:case 4:case 6:case 8:case 10:
                    echo 'genap';
                    break;
                case 1:case 3:case 5:case 7:case 9:
                    echo 'ganjil';
                    break;
                default:
                    echo 'bukan angka';
                    break;
            }
        }
        ?>
    </div>
</body>

</html>